require('dotenv').config();

module.exports = {
    "development": {
        "username": process.env.DB_USER,
        "password": process.env.DB_PASS,
        "database": process.env.DB_NAME,
        "host": process.env.DB_HOST,
        "dialect": "mysql",
        "operatorsAliases": 0
    },
    "test": {
        "username": process.env.DB_USER_TEST,
        "password": process.env.DB_PASS_TEST,
        "database": process.env.DB_NAME_TEST,
        "host": process.env.DB_HOST_TEST,
        "dialect": "mysql",
        "operatorsAliases": 0
    },
    "production": {
        "username": process.env.DB_USER_PRODUCTION,
        "password": process.env.DB_PASS_PRODUCTION,
        "database": process.env.DB_NAME_PRODUCTION,
        "host": process.env.DB_HOST_PRODUCTION,
        "dialect": "mysql",
        "operatorsAliases": 0
    }
}