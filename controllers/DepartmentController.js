const db = require('../models');

async function getAllDepartments(request, response) {
    try {
        const userList = await db.department.findAll();
        return response.status(200).json({
            status: 'OK',
            data: userList,
        });
    } catch (e) {
        console.error(e)
    }
    return response.status(500).json({
        status: 'Internal Server Error',
        errorMessage: 'Check output log for more details.',
    });
}

async function createDepartment(request, response) {
    const newDepartment = request.body;
    try {
        const newDepartmentData = await db.department.create(newDepartment);
        return response.status(201).json({
            status: 'Created',
            data: newDepartmentData,
        });
    } catch (e) {
        console.error(e)
    }
    return response.status(500).json({
        status: 'Internal Server Error',
        errorMessage: 'Check output log for more details.',
    });
}

async function findDepartment(request, response) {
    try {
        const departmentData = await db.department.findByPk(request.params.id);
        if (departmentData) {
            return response.status(200).json({
                status: 'FOUND',
                data: departmentData,
            });
        }
        return response.status(200).json({
            status: 'NOT FOUND',
            data: {},
        });
    } catch (e) {
        console.error(e)
    }
    return response.status(500).json({
        status: 'Internal Server Error',
        errorMessage: 'Check output log for more details.',
    });
}

async function deleteDepartment(request, response) {
    try {
        let departmentData = await db.department.findByPk(request.params.id);
        if (departmentData) {
            await db.department.destroy({
                where: {
                    id: request.params.id,
                }
            })
            return response.status(200).json({
                status: 'DELETED',
                data: departmentData,
            });
        }
        return response.status(200).json({
            status: 'NOT FOUND',
            data: {},
        });
    } catch (e) {
        console.error(e)
    }
    return response.status(500).json({
        status: 'Internal Server Error',
        errorMessage: 'Check output log for more details.',
    });
}

async function updateDepartment(request, response) {
    try {
        let departmentData = await db.department.findByPk(request.params.id);
        if (departmentData) {
            await db.department.update(request.body, {
                where: {
                    id: request.params.id
                }
            });
            let newDepartmentData = await db.department.findByPk(request.params.id);

            return response.status(200).json({
                status: 'UPDATED',
                data: newDepartmentData,
            });
        }
        return response.status(200).json({
            status: 'NOT FOUND',
            data: {},
        });
    } catch (e) {
        console.error(e)
    }
    return response.status(500).json({
        status: 'Internal Server Error',
        errorMessage: 'Check output log for more details.',
    });
}

module.exports = {
    getAllDepartments,
    createDepartment,
    findDepartment,
    deleteDepartment,
    updateDepartment,
}