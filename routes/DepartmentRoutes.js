const express = require('express');
const router = express.Router();
const DepartmentController = require('../controllers/DepartmentController.js');

// CREATE
router.post('/', (request, response) => {
    return DepartmentController.createDepartment(request, response);
});

// READ
router.get('/', (request, response) => {
    return DepartmentController.getAllDepartments(request, response);
});

// READ
router.get('/:id', (request, response) => {
    return DepartmentController.findDepartment(request, response);
});

// UPDATE
router.put('/:id', (request, response) => {
    return DepartmentController.updateDepartment(request, response);
});

// UPDATE
router.patch('/:id', (request, response) => {
    return DepartmentController.updateDepartment(request, response);
});

// DELETE
router.delete('/:id', (request, response) => {
    return DepartmentController.deleteDepartment(request, response);
});

module.exports = router;