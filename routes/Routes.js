const UserRoutes = require('../routes/UserRoutes.js');
const DepartmentRoutes = require('../routes/DepartmentRoutes.js');

module.exports = {
    UserRoutes,
    DepartmentRoutes,
}
