module.exports = (sequelize, DataTypes) => {
    const DepartmentModel = sequelize.define('department', {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
    });

    DepartmentModel.associate = (models) => {
        DepartmentModel.hasMany(models.user);
    };

    return DepartmentModel;
};